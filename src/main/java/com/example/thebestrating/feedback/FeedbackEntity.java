package com.example.thebestrating.feedback;

import com.example.thebestrating.city.CityEntity;
import com.vaadin.spring.annotation.VaadinSessionScope;

import javax.persistence.*;
import java.util.Objects;

/**
 * Class is entity of feedback
 * @author Nazar Dochylo
 * @author Andriy Drobot
 * @author Misha Morarash
 */
@Entity
@VaadinSessionScope
public class FeedbackEntity {

    /**Identification number of feedback*/
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**Message of feedback*/
    private String message;

    /**Grade of feedback*/
    private Double grade;


    /**Constructor creating new object
     * @see FeedbackEntity#FeedbackEntity(String, Double)
     */
    public FeedbackEntity() {}

    /**Constructor creating new object with specific values
     * @see FeedbackEntity#FeedbackEntity(String, Double) ()
     * @param message - feedback's message
     * @param grade - feedback's grade
     */
    public FeedbackEntity(String message, Double grade) {
        this.message = message;
        this.grade = grade;
    }

    /**
     * Obtaining a field value id that can be specified by a method {@link FeedbackEntity#setId(Long)}
     * @return value of field id
     */
    public Long getId() {
        return id;
    }

    /**Setting the value of the field id that can be obtained by a method {@link FeedbackEntity#getId()}
     * @param id identification number of feedback*/
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Obtaining a field message that can be specified by a method {@link FeedbackEntity#setMessage(String)}
     * @return value of field message
     */
    public String getMessage() {
        return message;
    }

    /**Setting the value of the field message that can be obtained by a method {@link FeedbackEntity#getMessage()}
     * @param message represents feedback's message*/
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Obtaining a field value grade that can be specified by a method {@link FeedbackEntity#setGrade(Double)}
     * @return value of field grade
     */
    public Double getGrade() {
        return grade;
    }

    /**Setting the value of the field message that can be obtained by a method {@link FeedbackEntity#getGrade()}
     * @param grade represents grade value from 1 to 5
     * */
    public void setGrade(Double grade) {
        this.grade = grade;
    }

    /**Comparison of objects of this class*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeedbackEntity that = (FeedbackEntity) o;
        return id.equals(that.id) &&
                Objects.equals(message, that.message) &&
                Objects.equals(grade, that.grade);
    }

    /**Getting hashcode of object*/
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**Converting object of class into string*/
    @Override
    public String toString() {
        return "FeedbackEntity{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", grade=" + grade +
                '}';
    }
}
