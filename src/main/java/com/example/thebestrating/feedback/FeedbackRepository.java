package com.example.thebestrating.feedback;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for feedback {@link com.example.thebestrating.city.CityEntity}
 * @author Andriy Drobot
 * @author Misha Morarash
 */
@Repository
public interface FeedbackRepository extends JpaRepository<FeedbackEntity, Long> {
}
