package com.example.thebestrating.feedback;

import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class implemented interface {@link com.example.thebestrating.feedback.FeedbackService}
 * @author Nazar Dochylo
 * @author Andriy Drobot
 * @author Misha Morarash
 **/
@Service
@VaadinSessionScope
public class FeedbackServiceImpl implements FeedbackService {

    /**Repository for feedback*/
    @Autowired
    private FeedbackRepository feedbackRepository;

    /**Obtaining feedback
     * @param id - feedback's identification number
     * @return found feedback*/
    @Override
    public FeedbackEntity getFeedback(Long id) {
        return feedbackRepository.getOne(id);
    }

    /**Creating feedback
     * @param message - feedback's message
     * @param grade - feedback's grade
     * @return created feedback object*/
    @Override
    public FeedbackEntity createFeedback(String message, Double grade) {
        return feedbackRepository.save(new FeedbackEntity(message,grade));
    }

    /**Creating feedback object that already exists
     * @param feedback - feedback object
     * @return saved feedback object*/
    @Override
    public FeedbackEntity save(FeedbackEntity feedback) {
        return feedbackRepository.save(feedback);
    }

    /**Updating feedback
     * @param id - feedback's identification number
     * @param newMsg - new message of feedback
     * @param grade - feedback's grade
     * @return updated feedback object*/
    @Override
    public FeedbackEntity update(Long id, String newMsg, Double grade) {
        FeedbackEntity feedback =  feedbackRepository.findById(id).get();
        feedback.setMessage(newMsg);
        feedback.setGrade(grade);
        return feedbackRepository.save(feedback);
    }

    /**Deleting city
     * @param id - feedback identification number*/
    @Override
    public void deleteById(Long id) {
        feedbackRepository.deleteById(id);
    }
}
