package com.example.thebestrating.feedback;

/**
 * Interface describing the functionality of the service
 * @author Nazar Dochylo
 * @author Andriy Drobot
 * @author Misha Mororash
 */
public interface FeedbackService {

    /**Obtaining feedback
     * @param id - feedback's identification number
     * @return found feedback*/
    FeedbackEntity getFeedback(Long id);

    /**Creating feedback
     * @param message - feedback's message
     * @param grade - feedback's grade
     * @return created feedback object*/
    FeedbackEntity createFeedback(String message, Double grade);

    /**Creating feedback object that already exists
     * @param feedback - feedback object
     * @return saved feedback object*/
    FeedbackEntity save(FeedbackEntity feedback);

    /**Updating feedback
     * @param id - feedback's identification number
     * @param newMsg - new message of feedback
     * @param grade - feedback's grade
     * @return updated feedback object*/
    FeedbackEntity update(Long id, String newMsg, Double grade);

    /**Deleting city
     * @param id - feedback identification number*/
    void deleteById(Long id);
}
