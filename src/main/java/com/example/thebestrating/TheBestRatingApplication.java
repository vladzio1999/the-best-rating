package com.example.thebestrating;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheBestRatingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheBestRatingApplication.class, args);
	}
}
