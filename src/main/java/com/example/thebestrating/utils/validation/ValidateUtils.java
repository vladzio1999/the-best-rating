package com.example.thebestrating.utils.validation;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.Validator;
import com.vaadin.data.ValueContext;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractField;
/**
 * Class for checking the correctness of fields
 * @author Vlad Tereschuk
 */

public class ValidateUtils {

    /**Method to set error for field if value is unacceptable
     * @param field - field to check
     * @param validator - validator which set some requirements for field
     */
    public static void addValidator(AbstractField field, Validator validator) {
        field.addValueChangeListener(event -> {
            ValidationResult result = validator.apply(event.getValue(), new ValueContext(field));

            if (result.isError()) {
                UserError error = new UserError(result.getErrorMessage());
                field.setComponentError(error);
            } else {
                field.setComponentError(null);
            }
        });
    }
}
