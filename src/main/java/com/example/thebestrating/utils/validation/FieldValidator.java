package com.example.thebestrating.utils.validation;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.Validator;
import com.vaadin.data.ValueContext;

/**
 * Class to determine the result of validation
 * @author Vlad Tereschuk
 */
public class FieldValidator implements Validator<String> {

    /**Field - the maximum number of characters*/
    private Integer max;

    /**Constructor - creating new object of class with a specific value
     * @param max - maximum number of characters {@link FieldValidator#max}
     */
    public FieldValidator(Integer max) {
        this.max = max;
    }

    /**Method to get validation result
     * @param value - value of the field being checked
     * @param valueContext - context of value
     * @return validation result {@link ValidationResult}
     */
    @Override
    public ValidationResult apply(String value, ValueContext valueContext) {
        if(value.length() == 0) {
            return ValidationResult.error("Поле не може бути порожнім");
        } else if (value.length() > max) {
            return ValidationResult.error("Максимальна кількість символів: " + max);
        } else {
            return ValidationResult.ok();
        }
    }
}
