package com.example.thebestrating.utils.image;

import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;
import org.vaadin.cropper.Cropper;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Date;

/**
 * Class to receive image from upload component
 * @see Upload
 * @author Vlad Tereschuk
 */
public class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

    /**Field for data of an image*/
    private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    /**Field for component to cropping
     * @see Cropper
     */
    private Cropper cropper;
    /**Field to keep data of original image in memory*/
    private BufferedImage originalImage;
    /**Field of caption text*/
    private Label cropLabel;

    /**
     * Constructor - creating new object with specific values
     * @param cropper - component to crop photo {@link ImageReceiver#cropper}
     * @param cropLabel - caption for crop component {@link ImageReceiver#cropLabel}
     */
    public ImageReceiver (Cropper cropper, Label cropLabel) {
        this.cropper = cropper;
        this.cropLabel = cropLabel;
    }

    /**
     * Method to get value of field {@link ImageReceiver#originalImage}
     * @return - data of original image
     */
    public BufferedImage getOriginalImage() {
        return originalImage;
    }



    /**
     * Method to receive uploading image being uploaded
     * @param fileName - name of image file
     * @param mimeType - type of file
     * @return image in stream of bytes
     */
    @Override
    public OutputStream receiveUpload(String fileName, String mimeType) {
        return byteArrayOutputStream;
    }

    /**
     * Method for processing the uploaded image.
     * Writes stream of bytes into buffered image {@link ImageReceiver#originalImage}
     * Set source with unique name for crop component {@link ImageReceiver#cropper}
     * @param succeededEvent - an event that occurs when a photo is uploaded successfully
     */
    @Override
    public void uploadSucceeded(Upload.SucceededEvent succeededEvent) {
        Date date = new Date();
        ByteArrayInputStream byteArrayInputStream;

        try {
            originalImage = ImageIO.read(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
            if (originalImage == null) {
                Notification notification = new Notification("Ви не вибрали фото");
                notification.show(Page.getCurrent());
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        cropper.setSource(new StreamResource(() -> byteArrayInputStream, "preview" + date.getTime()));
        cropLabel.setVisible(true);

        try {
            byteArrayOutputStream.close();
            byteArrayInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        succeededEvent.getUpload().setEnabled(false);
    }
}
