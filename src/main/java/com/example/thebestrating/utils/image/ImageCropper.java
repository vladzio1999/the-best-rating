package com.example.thebestrating.utils.image;

import org.vaadin.cropper.Cropper;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**Class to get cropped image
 * @author - Vlad Tereschuk
 */
public class ImageCropper {

    /**
     * Method to crop image in selected area.
     * Writes data of original image into buffered image, writes cropped image into stream of bytes
     * Gets data from stream of bytes and write them into byte array
     * @param cropper - component to select crop area
     * @param imageReceiver - value of class to receive image {@link ImageReceiver}
     * @return an array of bytes with data of cropped image
     */
    public static byte[] getCroppedImage(Cropper cropper, ImageReceiver imageReceiver) {
        byte[] imageInByte = null;

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        if(imageReceiver.getOriginalImage() != null) {
            try {
                BufferedImage bufferedImage = imageReceiver.getOriginalImage();
                ImageIO.write(bufferedImage.getSubimage(cropper.getCropSelection().getX(),
                                                        cropper.getCropSelection().getY(),
                                                        cropper.getCropSelection().getWidth(),
                                                        cropper.getCropSelection().getHeight()),
                                                        "jpg", byteArrayOutputStream);
                byteArrayOutputStream.flush();
                imageInByte = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return imageInByte;
    }
}
