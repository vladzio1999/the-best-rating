package com.example.thebestrating.utils.google;

/**
 * Class that represents exception which is thrown in {@link GoogleMapService}
 * @author Andriy Drobot
 */
public class GoogleMapServiceException extends RuntimeException {
    public GoogleMapServiceException(String message) {
        super(message);
    }
}
