package com.example.thebestrating.utils.google;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;

import java.io.IOException;

/**
 * Util class for sending request using google map api
 * @author Andriy Drobot
 */
@SpringComponent
@VaadinSessionScope
public class GoogleMapService {

    /**Gogle API key*/
    private final static String API_KEY = "AIzaSyBzwnrjxrzFuccQ8eiPdOJEi8GFJZB19uU";

    /**Default language*/
    private final static String LANGUAGE = "uk";

    /**Google API context, used when sending request to google maps service*/
    private GeoApiContext context;

    /**Constructor for initializing GeoApiContext*/
    public GoogleMapService() {
        context = new GeoApiContext.Builder()
                .apiKey(API_KEY)
                .build();
    }

    /**Getting address from LatLong object
     * @param location represents latitude longitude loction
     * @return string that represents address
     */
    public String toFormatedAddress(LatLon location) {
        try {
            LatLng latLng = new LatLng(location.getLat(), location.getLon());
            GeocodingResult[] results = GeocodingApi.reverseGeocode(context, latLng)
                    .language(LANGUAGE)
                    .await();
            return results.length == 0 ? null : results[0].formattedAddress;
        }
        catch (InterruptedException | ApiException | IOException ex) {
            throw new GoogleMapServiceException(ex.getMessage());
        }
    }

    /**Getting geocoding results from address
     * @param address represents location address
     * @return object that contain formatted address, LatLon object and etc.
     */
    public GeocodingResult toGeocodingResult(String address) {
        try {
            GeocodingResult[] results = GeocodingApi.geocode(context, address)
                    .language(LANGUAGE)
                    .await();
            return results.length == 0 ? null : results[0];
        }
        catch (InterruptedException | ApiException | IOException ex) {
            throw new GoogleMapServiceException(ex.getMessage());
        }
    }

    /**Initialing of GoogleMapWidget
     * @return initialized googleMap object
     */
    public GoogleMap googleMapInit() {
        return new GoogleMap(API_KEY, null, "ukrainian");
    }
}
