package com.example.thebestrating.view;

import com.example.thebestrating.city.CityEntity;
import com.example.thebestrating.city.CityService;
import com.example.thebestrating.city.CityServiceImpl;
import com.example.thebestrating.hotel.HotelEntity;
import com.example.thebestrating.hotel.HotelService;
import com.example.thebestrating.ui.MenuNavigation;
import com.example.thebestrating.utils.image.ImageCropper;
import com.example.thebestrating.utils.image.ImageReceiver;
import com.example.thebestrating.utils.validation.FieldValidator;
import com.example.thebestrating.utils.google.GoogleMapService;
import com.example.thebestrating.utils.validation.ValidateUtils;
import com.example.thebestrating.hotel.HotelServiceImpl;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.events.MapClickListener;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.vaadin.cropper.Cropper;

import java.io.IOException;
import java.util.List;

/**
 * Class of page for adding cities
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
@SpringComponent
@VaadinSessionScope
public class AddHotelView extends VerticalLayout implements Component {

    /**Service to perform operations with {@link HotelEntity}*/
    @Autowired
    private HotelService hotelService;

    /**Service to perform operations with {@link com.example.thebestrating.city.CityEntity}*/
    @Autowired
    private CityService cityService;

    /**Service to perform operations with {@link GoogleMapService}*/
    @Autowired
    private GoogleMapService googleMapService;

    /**Map*/
    private GoogleMap googleMap;
    /**Name of hotel*/
    private TextField nameField;
    /**Description of hotel*/
    private TextArea descriptionField;
    /**Component to select city*/
    private ComboBox<CityEntity> selectCity;
    /**Button to add hotel*/
    private Button addHotelButton;
    /**Button to search address of hotel*/
    private Button searchAddressBtn;
    /**Field for address of hotel*/
    private TextField addressField;
    /**Entity of hotel*/
    private HotelEntity hotelEntity;
    /**Button for editing hotel*/
    private Button editHotelButton;
    /**Button to cancel editin hotel*/
    private Button cancelButton;
    /**Field for elements to cancel editing*/
    private CssLayout editCancelLayout;

    /**Method to initialize page */
    public void init() {
        removeAllComponents();
        setMargin(false);

        final HorizontalLayout mainLayout = new HorizontalLayout();
        mainLayout.setSizeFull();

        Label addNewHotelLabel = new Label("Додайте свій готель");
        addNewHotelLabel.setStyleName(ValoTheme.LABEL_H3);

        final VerticalLayout descriptionLayout = new VerticalLayout();
        descriptionLayout.setSizeFull();

        final VerticalLayout cropImageLayout = new VerticalLayout();

        nameField = new TextField();
        nameField.setWidth("250");
        nameField.setCaption("Введіть назву готелю");
        nameField.addValueChangeListener(valueChangeEvent -> {
            if (checkFields()) {
                addHotelButton.setEnabled(true);
                editHotelButton.setEnabled(true);
            } else {
                addHotelButton.setEnabled(false);
                editHotelButton.setEnabled(false);
            }
        });

        descriptionField = new TextArea();
        descriptionField.setWidth("500");
        descriptionField.setCaption("Добавте опис готелю");
        descriptionField.addValueChangeListener(valueChangeEvent -> {
            if (checkFields()) {
                addHotelButton.setEnabled(true);
                editHotelButton.setEnabled(true);
            } else {
                addHotelButton.setEnabled(false);
                editHotelButton.setEnabled(false);
            }
        });

        cancelButton = new Button();
        cancelButton.setCaption("Назад");
        cancelButton.setVisible(false);
        cancelButton.addClickListener(e->cancelButtonClickFunc());

        editHotelButton = new Button();
        editHotelButton.setEnabled(false);
        editHotelButton.setVisible(false);
        editHotelButton.setCaption("Зберегти зміни");


        editCancelLayout = new CssLayout();
        editCancelLayout.setVisible(false);
        editCancelLayout.addComponents(editHotelButton,cancelButton);

        addressField = new TextField("Ваша адреса");
        addressField.setWidth(450, Unit.PIXELS);
        addressField.addShortcutListener(new ShortcutListener("Search",
                ShortcutAction.KeyCode.ENTER, null) {

            @Override
            public void handleAction(Object o, Object o1) {
                searchAddressBtn.click();
            }
        });
        addressField.addValueChangeListener(valueChangeEvent -> {
            if(addressField.isEmpty()) {
                searchAddressBtn.setEnabled(false);
            }

            searchAddressBtn.setEnabled(true);
            addHotelButton.setEnabled(false);
            editHotelButton.setEnabled(false);
        });


        Cropper cropper = new Cropper(null);
        cropper.setSizeFull();
        cropper.setAspectRatio(1);

        Label cropLabel = new Label("Виділіть необхідну область фото");
        cropLabel.setVisible(false);

        ImageReceiver imageReceiver = new ImageReceiver(cropper, cropLabel);

        Upload upload = new Upload("Ви можете завантажити фото готелю", imageReceiver);
        upload.setImmediateMode(false);
        upload.addSucceededListener(imageReceiver);
        upload.setButtonCaption("Завантажити");
        upload.setAcceptMimeTypes("image/jpeg");

        List<CityEntity> city = cityService.getAllCities();

        selectCity = new ComboBox<>("Оберіть місто:");
        selectCity.setPlaceholder("Місто не вибране");
        selectCity.setTextInputAllowed(true);
        selectCity.setItems(cityService.getAllCities());
        selectCity.setItemCaptionGenerator(CityEntity::getName);
        selectCity.setStyleName("margin-top: 20px;");
        selectCity.addValueChangeListener(valueChangeEvent -> {
            if (!nameField.isEmpty() && valueChangeEvent.getValue() != null) {
                setMarkerOnMap(valueChangeEvent.getValue().getName());
            }

            if (checkFields()) {
                addHotelButton.setEnabled(true);
                editHotelButton.setEnabled(true);
            } else {
                addHotelButton.setEnabled(false);
                editHotelButton.setEnabled(false);
            }
        });

        addHotelButton = new Button();
        addHotelButton.setEnabled(false);
        addHotelButton.setCaption("Зберегти");
        addHotelButton.addClickListener(e -> addHotelButtonClickListener(nameField, descriptionField, selectCity,
                cropper, imageReceiver, googleMap, addressField));
        editHotelButton.addClickListener(e->editButtonClickFunc(nameField, descriptionField, selectCity,
                cropper, imageReceiver, googleMap, addressField));


        googleMap = googleMapService.googleMapInit();
        googleMap.setHeight("300px");
        googleMap.setWidth("600px");
        GoogleMapMarker startMarker = new GoogleMapMarker();
        startMarker.setPosition(new LatLon(48.2949, 25.94034));
        startMarker.setDraggable(true);
        googleMap.setCenter(startMarker.getPosition());
        googleMap.addMarker(startMarker);

        googleMap.addMapClickListener((MapClickListener) latLon -> {
            googleMap.getMarkers().clear();
            googleMap.addMarker("Ваш готель",
                    latLon, true, null);
            addressField.setValue(googleMapService.toFormatedAddress(latLon));
            searchAddressBtn.setEnabled(false);
            if(checkFields()) {
                addHotelButton.setEnabled(true);
                editHotelButton.setEnabled(true);
            }

        });
        googleMap.addMarkerDragListener((googleMapMarker, latLon) -> {
            addressField.setValue(googleMapService.toFormatedAddress(googleMapMarker.getPosition()));
            searchAddressBtn.setEnabled(false);
            if(checkFields()) {
                addHotelButton.setEnabled(true);
                editHotelButton.setEnabled(true);
            }

        });

        searchAddressBtn = new Button("Знайти адресу");
        searchAddressBtn.setEnabled(false);
        searchAddressBtn.addClickListener(e -> {
            setMarkerOnMap(addressField.getValue());

            if (checkFields()) {
                addHotelButton.setEnabled(true);
                editHotelButton.setEnabled(true);
            }
        });

        selectCity.addValueChangeListener(valueChangeEvent -> {
            if (!selectCity.isEmpty()) {
                addHotelButton.setEnabled(true);
            } else {
                addHotelButton.setEnabled(false);
            }
            if (descriptionField.isEmpty()) {
                addHotelButton.setEnabled(false);
            }
            if (nameField.isEmpty()) {
                addHotelButton.setEnabled(false);
            }
        });

        if(hotelEntity != null){
            nameField.setValue(hotelEntity.getName());
            descriptionField.setValue(hotelEntity.getDescription());
            addressField.setValue(hotelEntity.getAddress());
            selectCity.setValue(hotelEntity.getCity());
            addNewHotelLabel.setValue("Внесіть зміни");
            editHotelButton.setVisible(true);
            cancelButton.setVisible(true);
            editCancelLayout.setVisible(true);
            addHotelButton.setVisible(false);
            searchAddressBtn.click();
        }

        AbsoluteLayout searchLayout = new AbsoluteLayout();
        searchLayout.setWidth(600, Unit.PIXELS);
        searchLayout.setHeight(60, Unit.PIXELS);
        searchLayout.addComponent(addressField, "left: 0px; bottom: 0px");
        searchLayout.addComponent(searchAddressBtn, "bottom: 0px; right: 0px");

        ValidateUtils.addValidator(nameField, new FieldValidator(30));
        ValidateUtils.addValidator(descriptionField, new FieldValidator(255));
        descriptionLayout.addComponents(addNewHotelLabel, nameField, descriptionField,
                upload, selectCity, searchLayout, googleMap, addHotelButton, editCancelLayout);
        cropImageLayout.addComponents(cropLabel, cropper);
        mainLayout.addComponents(descriptionLayout, cropImageLayout);
        addComponent(mainLayout);
    }

    /**
     * Method to check values of fields
     * @return result of the check
     */
    public boolean checkFields() {
        return !nameField.isEmpty() && !descriptionField.isEmpty()
                && !selectCity.isEmpty() && !addressField.isEmpty();
    }

    /**
     * Method to add hotel
     * @param name - name of hotel
     * @param description - description of hotel
     * @param selectCity - component to select city
     * @param cropper - crop component
     * @param receiver - image receiver
     * @param googleMap - map
     * @param addressField - address of hotel
     */
    private void addHotelButtonClickListener(TextField name, TextArea description, ComboBox<CityEntity> selectCity,
                                             Cropper cropper, ImageReceiver receiver, GoogleMap googleMap,
                                             TextField addressField) {
        if (name.getComponentError() == null && description.getComponentError() == null && (!selectCity.isEmpty())) {

            hotelService.createHotel(name.getValue(), description.getValue(),
                    selectCity.getValue().getName(),
                    ImageCropper.getCroppedImage(cropper, receiver),
                    googleMap.getMarkers().iterator().next().getPosition(),
                    addressField.getValue());
            name.setValue("");
            description.setValue("");
            selectCity.setValue(null);
            ((MenuNavigation)UI.getCurrent()).goToAllHotels();
        } else {
            Notification notification = new Notification("Заповніть правильно усі поля!");
            notification.show(Page.getCurrent());
        }
    }

    /**
     * Obtaining a field value hotelEntity that can be specified by a method
     * {@link AddHotelView#setHotelEntity(HotelEntity)}
     * @return value of field cityEntity
     */
    public HotelEntity getHotelEntity() {
        return hotelEntity;
    }

    /**
     * Setting the value of the field hotelEntity that can be obtained by a method
     * {@link AddHotelView#getHotelEntity()}
     * @param hotelEntity - hotel entity
     */
    public void setHotelEntity(HotelEntity hotelEntity) {
        this.hotelEntity = hotelEntity;
    }

    /**
     * Method to edit hotel
     * @param name - name of hotel
     * @param description - description of hotel
     * @param selectCity - component to select city
     * @param cropper - crop component
     * @param receiver - image receiver
     * @param googleMap - map
     * @param addressField - address of hotel
     */
    public void editButtonClickFunc(TextField name, TextArea description, ComboBox<CityEntity> selectCity,
                                    Cropper cropper, ImageReceiver receiver, GoogleMap googleMap,
                                    TextField addressField){

        if (nameField.getComponentError() == null &&
                descriptionField.getComponentError() == null && (!selectCity.isEmpty())) {

            hotelEntity.setName(name.getValue());
            hotelEntity.setDescription(description.getValue());
            hotelEntity.setCity(selectCity.getValue());
            hotelEntity.setAddress(addressField.getValue());
            hotelEntity.setLatLon(googleMap.getMarkers().iterator().next().getPosition());
            if(ImageCropper.getCroppedImage(cropper, receiver)!=null) {
                hotelEntity.setImage(ImageCropper.getCroppedImage(cropper, receiver));
            }
            hotelService.save(hotelEntity);
            hotelEntity = null;
            UI.getCurrent().getPage().reload();
        } else {
            Notification notification = new Notification("Заповніть правильно усі поля!");
            notification.show(Page.getCurrent());
        }
    }

    /**Method to cancel editing hotel*/
    public void cancelButtonClickFunc(){
        hotelEntity = null;
        UI.getCurrent().getPage().reload();
    }

    /**
     * Method to search hotel on map
     * @param cityName - name of city
     */
    private void setMarkerOnMap(String cityName) {
        GeocodingResult result = null;

        try {
            result = googleMapService.toGeocodingResult(cityName
                    + ", готель " + nameField.getValue());
        } catch (Exception ex) {
            Notification notification = new Notification("Не вдалось здійснити пошук. Повторіть спробу");
            notification.show(Page.getCurrent());
        }


        if (result == null) {
            addressField.clear();
            searchAddressBtn.setEnabled(false);
            Notification notification = new Notification("Не можемо знайти такої адреси");
            notification.show(Page.getCurrent());
            return;
        }

        googleMap.getMarkers().clear();
        addressField.setValue(result.formattedAddress);
        googleMap.addMarker("Ваш готель", new LatLon(result.geometry.location.lat,
                result.geometry.location.lng), true, null);
        googleMap.setCenter(googleMap.getMarkers().iterator().next().getPosition());
        searchAddressBtn.setEnabled(false);
    }
}
