package com.example.thebestrating.view;

import com.example.thebestrating.city.CityEntity;
import com.example.thebestrating.city.CityService;
import com.example.thebestrating.ui.MenuNavigation;
import com.example.thebestrating.utils.image.ImageCropper;
import com.example.thebestrating.utils.image.ImageReceiver;
import com.example.thebestrating.utils.validation.FieldValidator;
import com.example.thebestrating.utils.validation.ValidateUtils;
import com.vaadin.server.*;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.cropper.Cropper;

/**
 * Class of page for adding cities
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 */
@SpringComponent
@VaadinSessionScope
public class AddCityView extends VerticalLayout implements Component {

    /**Service to perfom operations with {@link com.example.thebestrating.city.CityEntity}*/
    @Autowired
    private CityService cityService;

    /**Entity of city*/
    private CityEntity cityEntity;
    /**Button to editing city*/
    private Button editCityButton;
    /**Button to canceling editing city*/
    private Button cancelButton;
    /**Field for editing elements*/
    private CssLayout editCancelLayout;

    /**Method to initialize page*/
    public void init() {
        removeAllComponents();
        setMargin(false);

        final HorizontalLayout mainLayout = new HorizontalLayout();
        mainLayout.setSizeFull();
        mainLayout.setMargin(false);

        final VerticalLayout descriptionLayout = new VerticalLayout();
        descriptionLayout.setSizeFull();

        final VerticalLayout cropImageLayout = new VerticalLayout();
        Label addNewCityLabel = new Label("Додайте місто");
        addNewCityLabel.setStyleName(ValoTheme.LABEL_H3);

        final TextField nameField = new TextField();
        nameField.setWidth("250");
        nameField.setCaption("Введіть назву міста");

        final TextArea descriptionField = new TextArea();
        descriptionField.setWidth("500");
        descriptionField.setCaption("Добавте опис міста");

        Cropper cropper = new Cropper(null);
        cropper.setSizeFull();
        cropper.setAspectRatio(1);

        Label cropLabel = new Label("Виділіть необхідну область фото");
        cropLabel.setVisible(false);

        ImageReceiver imageReceiver = new ImageReceiver(cropper, cropLabel);

        Upload upload = new Upload("Ви можете завантажити фото міста", imageReceiver);
        upload.setImmediateMode(false);
        upload.addSucceededListener(imageReceiver);
        upload.setButtonCaption("Завантажити");
        upload.setAcceptMimeTypes("image/jpeg");

        Button addCityButton = new Button();
        addCityButton.setEnabled(false);
        addCityButton.setCaption("Зберегти");
        addCityButton.addClickListener(e -> addCityButtonClickListener(nameField, descriptionField,
                cropper, imageReceiver));
        cancelButton = new Button();
        cancelButton.setCaption("Назад");
        cancelButton.setVisible(false);
        cancelButton.addClickListener(e->cancelButtonClickFunc());

        editCityButton = new Button();
        editCityButton.setEnabled(false);
        editCityButton.setVisible(false);
        editCityButton.setCaption("Зберегти зміни");
        editCityButton.addClickListener(e->editButtonClickFunc(nameField, descriptionField,
                cropper, imageReceiver));

        editCancelLayout = new CssLayout();
        editCancelLayout.setVisible(false);
        editCancelLayout.addComponents(editCityButton,cancelButton);

        nameField.addValueChangeListener(valueChangeEvent -> {
            if (nameField.isEmpty()) {
                addCityButton.setEnabled(false);
                editCityButton.setEnabled(false);
            } else if (!descriptionField.isEmpty()) {
                addCityButton.setEnabled(true);
                editCityButton.setEnabled(true);
            }
        });

        descriptionField.addValueChangeListener(valueChangeEvent -> {
            if (descriptionField.isEmpty()) {
                addCityButton.setEnabled(false);
                editCityButton.setEnabled(false);
            } else if (!nameField.isEmpty()) {
                addCityButton.setEnabled(true);
                editCityButton.setEnabled(true);
            }
        });

        if(cityEntity != null){
            nameField.setValue(cityEntity.getName());
            descriptionField.setValue(cityEntity.getDescription());
            addNewCityLabel.setValue("Внесіть зміни");
            editCityButton.setVisible(true);
            cancelButton.setVisible(true);
            editCancelLayout.setVisible(true);
            addCityButton.setVisible(false);
        }

        ValidateUtils.addValidator(nameField, new FieldValidator(30));
        ValidateUtils.addValidator(descriptionField, new FieldValidator(255));
        descriptionLayout.addComponents(addNewCityLabel, nameField,
                descriptionField, upload, addCityButton, editCancelLayout);

        cropImageLayout.addComponents(cropLabel, cropper);
        mainLayout.addComponents(descriptionLayout, cropImageLayout);
        addComponent(mainLayout);
    }

    /**Method to add city
     * @param name - name of city
     * @param description - description of city
     * @param cropper - crop component
     * @param receiver - image receiver
     */
    private void addCityButtonClickListener(TextField name, TextArea description, Cropper cropper,
                                            ImageReceiver receiver) {
        if (name.getComponentError() == null && description.getComponentError() == null) {
            cityService.createCity(name.getValue(), description.getValue(),
                    ImageCropper.getCroppedImage(cropper, receiver));
            name.setValue("");
            description.setValue("");
            ((MenuNavigation)UI.getCurrent()).goToAllCities();
        } else {
            Notification notification = new Notification("Заповніть правильно усі поля!");
            notification.show(Page.getCurrent());
        }
    }

    /**
     * Obtaining a field value cityEntity that can be specified by a method
     * {@link AddCityView#setCityEntity(CityEntity)}
     * @return value of field cityEntity
     */
    public CityEntity getCityEntity() {
        return cityEntity;
    }

    /**
     * Setting the value of the field cityEntity that can be obtained by a method
     * {@link AddCityView#getCityEntity()}
     * @param cityEntity - city object that shoul ne represented on view
     */
    public void setCityEntity(CityEntity cityEntity) {
        this.cityEntity = cityEntity;
    }

    /**
     * Method to edit city
     * @param name - name of city
     * @param description - description of city
     * @param cropper - crop component
     * @param receiver - image receiver
     */
    public void editButtonClickFunc(TextField name, TextArea description, Cropper cropper,
                                    ImageReceiver receiver){

        if (name.getComponentError() == null && description.getComponentError() == null) {

            cityEntity.setDescription(description.getValue());
            cityEntity.setName(name.getValue());
            if(ImageCropper.getCroppedImage(cropper, receiver)!=null) {
                cityEntity.setImage(ImageCropper.getCroppedImage(cropper, receiver));
            }
            cityService.save(cityEntity);
            name.setValue("");
            description.setValue("");
            cityEntity = null;
            ((MenuNavigation)UI.getCurrent()).goToAllCities();
        } else {
            Notification notification = new Notification("Заповніть правильно усі поля!");
            notification.show(Page.getCurrent());
        }
    }

    /**Method to cancel editing city*/
    public void cancelButtonClickFunc(){
        cityEntity = null;
        ((MenuNavigation)UI.getCurrent()).goToAllCities();
    }
}
