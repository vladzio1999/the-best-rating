package com.example.thebestrating.view;

import com.example.thebestrating.feedback.FeedbackEntity;
import com.example.thebestrating.hotel.HotelEntity;
import com.example.thebestrating.hotel.HotelService;
import com.example.thebestrating.ui.MenuNavigation;
import com.google.maps.errors.ApiException;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.teemu.ratingstars.RatingStars;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Class for showing hotels for given city
 * @author Nazar Dochylo
 * @author Andriy Drobot
 * @author Vlad Tereschuk
 */
@VaadinSessionScope
@SpringComponent
public class HotelViewForCity extends VerticalLayout implements Component {

    /**Service to perform operations with {@link HotelEntity}*/
    @Autowired
    private HotelService hotelService;

    /**Object to show reviews for current {@link HotelEntity}*/
    @Autowired
    private HotelInfoView hotelInfoView;

    /**View to change current page to the {@link AddHotelView}*/
    @Autowired
    private AddHotelView addHotelView;

    /**List that stores {@link HotelEntity} for a given {@link com.example.thebestrating.city.CityEntity}*/
    private List<HotelEntity> cityHotels;

    /**Constant for set number of {@link HotelEntity} per one page*/
    private final static int MAX_HOTELS_ON_PAGE = 3;

    /**Variable which store number of current page*/
    private String currentPage;

    /**Variable which store city name*/
    private String cityName;

    /**Variable that determine if {@link HotelInfoView} is open*/
    private boolean isOpen = false;

    /**Array that store buttons*/
    private Button[] pageButton = new Button[3];


    public void init() {
        currentPage = "1";
        this.setMargin(false);
        removeAllComponents();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setMargin(new MarginInfo(false, true, true, true));
        HorizontalLayout pagesLayout = new HorizontalLayout();
        for (int i = 0; i < 3; i++) {
            pageButton[i] = new Button();
            pageButton[i].setVisible(false);
            pageButton[i].setCaption(String.valueOf(i + 1));
            pageButton[i].addStyleName(ValoTheme.BUTTON_TINY);
            pageButton[i].addClickListener(clickEvent -> {
                if (!clickEvent.getButton().getCaption().equals(currentPage)) {
                    currentPage = clickEvent.getButton().getCaption();
                    contentLayout.removeAllComponents();
                    setLayoutContent(contentLayout);
                    refactorPageButton(clickEvent.getButton());
                }});

        }
        drawPageButton(pageButton);
        pagesLayout.addComponents(pageButton);
        setLayoutContent(contentLayout);
        setDefaultComponentAlignment(Alignment.TOP_CENTER);
        addComponents(contentLayout, pagesLayout);
    }

    /**Getter for cityHotel
     * @return list of represented city's hotels  */
    public List<HotelEntity> getCityHotels() {
        return cityHotels;
    }

    /**Setter for cityHotel
     * @param cityHotels - hotels that should be represented*/
    public void setCityHotels(List<HotelEntity> cityHotels) {
        this.cityHotels = cityHotels;
    }

    /**Getter for cityName
     * @return name of city that should be represented*/
    public String getCityName() {
        return cityName;
    }

    /**Setter for cityName
     * @param cityName  - name of the city that should be represented on the view*/
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**Function for loading vaadin UI components to a page */
    private void setLayoutContent(VerticalLayout contentLayout) {
        Button backButton = new Button("Всі міста" , e -> ((MenuNavigation)UI.getCurrent()).goToAllCities());
        backButton.setIcon(VaadinIcons.ARROW_CIRCLE_LEFT);

        contentLayout.addComponent(backButton);
        int currentPage = Integer.parseInt(this.currentPage);

        cityHotels.stream()
                .skip((currentPage - 1) * MAX_HOTELS_ON_PAGE)
                .limit(MAX_HOTELS_ON_PAGE)
                .forEach(hotel ->  {
                    VerticalLayout innerLayout = new VerticalLayout();
                    VerticalLayout outerLayout = new VerticalLayout();
                    HorizontalLayout infoLayout = new HorizontalLayout();
                    HorizontalLayout panelLayout = new HorizontalLayout();
                    VerticalLayout ratingAndAddressLayout = new VerticalLayout();
                    outerLayout.setMargin(false);
                    panelLayout.setMargin(false);
                    innerLayout.setSpacing(false);
                    innerLayout.setMargin(false);
                    innerLayout.setSizeFull();
                    panelLayout.setWidth("100%");
                    ratingAndAddressLayout.setMargin(new MarginInfo(true, true, false, false));
                    ratingAndAddressLayout.setSizeFull();
                    ratingAndAddressLayout.setSpacing(false);

                    infoLayout.setMargin(new MarginInfo(false, false, true, false));

                    Panel panel = new Panel();

                    Label hotelName = new Label(hotel.getName());
                    hotelName.setStyleName(ValoTheme.LABEL_H2);

                    TextArea descriptionArea = new TextArea();
                    descriptionArea.setWidth("100%");
                    descriptionArea.setHeight("90");
                    descriptionArea.setValue(hotel.getDescription());
                    descriptionArea.setReadOnly(true);
                    descriptionArea.setCaption("Опис:");

                    TextArea hotelAddress = new TextArea();
                    hotelAddress.setReadOnly(true);
                    hotelAddress.setValue(hotel.getAddress());
                    hotelAddress.setCaption("Адреса:");
                    hotelAddress.setStyleName(ValoTheme.TEXTAREA_BORDERLESS);
                    hotelAddress.setWidth("100%");
                    hotelAddress.setHeight("80");

                    RatingStars ratingStars = new RatingStars();
                    ratingStars.setCaption("Середня оцінка: ");
                    ratingStars.setAnimated(false);
                    ratingStars.setReadOnly(true);
                    ratingStars.setMaxValue(5);
                    ratingStars.setValue(hotel.getAvgRating());

                    Image image = new Image();
                    image.setSizeFull();

                    if (hotel.getImage() == null) {
                        String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
                        FileResource resource = new FileResource(new File(basepath
                                + "/WEB-INF/images/no_image_available.jpg"));
                        image.setSource(resource);
                    } else {
                        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(hotel.getImage());
                        StreamResource resource = new StreamResource(() -> byteArrayInputStream, hotel.getName());
                        image.setSource(resource);
                        try {
                            byteArrayInputStream.close();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }

                    panel.addClickListener(clickEvent -> panelClickListener(contentLayout, outerLayout, hotel) );


                    innerLayout.addComponents(hotelName, descriptionArea);
                    ratingAndAddressLayout.addComponents(ratingStars, hotelAddress);
                    ratingAndAddressLayout.setComponentAlignment(ratingStars, Alignment.MIDDLE_RIGHT);
                    infoLayout.addComponentsAndExpand(innerLayout, ratingAndAddressLayout);
                    panelLayout.addComponents(image, infoLayout);
                    panelLayout.setExpandRatio(image, 0.2f);
                    panelLayout.setExpandRatio(infoLayout, 0.9f);
                    panel.setContent(panelLayout);
                    outerLayout.addComponent(panel);
                    contentLayout.addComponent(outerLayout);
                });
    }


    private void refactorPageButton(Button currentButton) {
        int btnIndex = Integer.parseInt(currentButton.getCaption());

        if (currentButton == pageButton[2]) {
            int hotelsCount = cityHotels.size();

            if (hotelsCount <= btnIndex * MAX_HOTELS_ON_PAGE) {
                return;
            }

            for (Button btn : pageButton) {
                int index = Integer.parseInt(btn.getCaption());
                btn.setCaption(String.valueOf(index + 1));
            }

            pageButton[1].focus();
        }
        if (currentButton == pageButton[0] && btnIndex != 1) {
            for (Button btn : pageButton) {
                int index = Integer.parseInt(btn.getCaption());
                btn.setCaption(String.valueOf(index - 1));
            }
            pageButton[1].focus();
        }
    }

    /**Function for drawing page buttons*/
    private void drawPageButton(Button[] pageButton) {
        if (cityHotels.size() <= MAX_HOTELS_ON_PAGE) {
            return;
        }

        for (int i = 0; i < 3; i++) {
            if (cityHotels.size() > (Integer.parseInt(pageButton[i].getCaption())-1)
                    * MAX_HOTELS_ON_PAGE) {
                pageButton[i].setVisible(true);
            }
        }
    }

    /**Function for opening {@link HotelInfoView} page*/
    private void panelClickListener(VerticalLayout contentLayout, VerticalLayout outerLayout, HotelEntity hotel) {
        if (!isOpen) {
            isOpen = true;
            hotelInfoView.setHotel(hotel);
            hotelInfoView.init();
            outerLayout.addComponent(hotelInfoView);
        } else {
            isOpen = false;
            setCityHotels(hotelService.getHotelsByCity(cityName));
            outerLayout.removeComponent(hotelInfoView);
            contentLayout.removeAllComponents();
            setLayoutContent(contentLayout);
        }
    }
}
