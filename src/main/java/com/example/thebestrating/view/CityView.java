package com.example.thebestrating.view;

import com.example.thebestrating.city.CityEntity;
import com.example.thebestrating.city.CityService;
import com.example.thebestrating.hotel.HotelEntity;
import com.example.thebestrating.hotel.HotelService;
import com.example.thebestrating.ui.MenuNavigation;
import com.google.maps.errors.ApiException;
import com.vaadin.server.FileResource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.teemu.ratingstars.RatingStars;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

/**
 * Class for showing hotels for given city
 * @author Nazar Dochylo
 * @author Andriy Drobot
 * @author Vlad Tereschuk
 */
@SpringComponent
@VaadinSessionScope
public class CityView extends VerticalLayout implements Component {

    /**Service to perform operations with {@link CityEntity}*/
    @Autowired
    private CityService cityService;

    /**Object to show hotels for current city}*/
    @Autowired
    private HotelViewForCity hotelViewForCity;

    /**Service to perform operations with {@link HotelEntity}*/
    @Autowired
    private HotelService hotelService;

    /**Object to show reviews for current {@link CityEntity}*/
    @Autowired
    private CityInfoView cityInfoView;

    /**View to change current page to the {@link AddCityView}*/
    @Autowired
    private AddCityView addCityView;

    /**Constant for set number of {@link CityEntity} per one page*/
    private final static int MAX_CITIES_ON_PAGE = 3;

    /**Variable that store number of current page*/
    private String currentPage;

    /**Variable that determine if {@link HotelInfoView} is open*/
    private boolean isOpen = false;

    private CityEntity cityEntity;

    /**Array that store buttons*/
    private Button[] pageButton = new Button[3];

    /**Function that initializes the class, configures the components and call
     * {@link CityView#setLayoutContent(VerticalLayout)}   function that load UI components
     * to a given layout*/
    public void init() {
        currentPage = "1";
        this.setMargin(false);
        removeAllComponents();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setMargin(new MarginInfo(false, true, true, true));
        HorizontalLayout pagesLayout = new HorizontalLayout();
        for (int i = 0; i < 3; i++) {
            pageButton[i] = new Button();
            pageButton[i].setVisible(false);
            pageButton[i].setCaption(String.valueOf(i + 1));
            pageButton[i].addStyleName(ValoTheme.BUTTON_TINY);
            pageButton[i].addClickListener(clickEvent -> {
                if (!clickEvent.getButton().getCaption().equals(currentPage)) {
                    currentPage = clickEvent.getButton().getCaption();
                    contentLayout.removeAllComponents();
                    setLayoutContent(contentLayout);
                    refactorPageButton(clickEvent.getButton());
                }});
        }
        drawPageButton(pageButton);
        pagesLayout.addComponents(pageButton);
        setLayoutContent(contentLayout);
        setDefaultComponentAlignment(Alignment.TOP_CENTER);
        addComponents(contentLayout, pagesLayout);
    }

    /**Function for loading vaadin UI components to a page */
    private void setLayoutContent(VerticalLayout contentLayout) {
        if (cityService.getAllCities().isEmpty()) {
            Label emptyMassageLabel = new Label("Міст ще немає.");
            Label createLabel = new Label("Добавте.");

            emptyMassageLabel.addStyleName(ValoTheme.LABEL_H3);
            createLabel.addStyleName(ValoTheme.LABEL_H3);
            createLabel.addStyleName(ValoTheme.LABEL_COLORED);

            HorizontalLayout clickLayout = new HorizontalLayout();
            clickLayout.addComponent(createLabel);
            clickLayout.addLayoutClickListener(e-> ((MenuNavigation)UI.getCurrent()).goToAddCity());
            clickLayout.setComponentAlignment(createLabel, Alignment.MIDDLE_CENTER);

            HorizontalLayout messageLayout = new HorizontalLayout();
            messageLayout.addComponents(emptyMassageLabel,clickLayout);

            addComponent(messageLayout);
            setComponentAlignment(messageLayout, Alignment.MIDDLE_CENTER);
        } else {
            int currentPage = Integer.parseInt(this.currentPage);

            cityService.getAllCities()
                    .stream()
                    .skip((currentPage - 1) * MAX_CITIES_ON_PAGE)
                    .limit(MAX_CITIES_ON_PAGE)
                    .forEach(city -> {
                        VerticalLayout innerLayout = new VerticalLayout();
                        VerticalLayout outerLayout = new VerticalLayout();
                        HorizontalLayout cityNameAndAVGRatingLayout = new HorizontalLayout();
                        HorizontalLayout panelLayout = new HorizontalLayout();
                        outerLayout.setMargin(false);
                        panelLayout.setMargin(false);
                        innerLayout.setSpacing(false);
                        innerLayout.setMargin(false);
                        panelLayout.setWidth("100%");
                        panelLayout.setMargin(new MarginInfo(false, true, false, false));

                        Panel panel = new Panel();
                        Label cityName = new Label(city.getName());
                        cityName.setStyleName(ValoTheme.LABEL_H2);

                        Button viewHotels = new Button("Показати готелі...");
                        viewHotels.setStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

                        RatingStars ratingStars = new RatingStars();
                        ratingStars.setMaxValue(5);
                        ratingStars.setReadOnly(true);
                        ratingStars.setValue(city.getAvgRating());
                        ratingStars.setAnimated(false);
                        ratingStars.setCaption("Середня оцінка: ");

                        cityNameAndAVGRatingLayout.addComponentsAndExpand(cityName);
                        cityNameAndAVGRatingLayout.addComponent(ratingStars);

                        viewHotels.addClickListener(e -> {
                            hotelViewForCity.setCityHotels(hotelService.getHotelsByCity(city.getName()));
                            hotelViewForCity.setCityName(city.getName());
                            hotelViewForCity.init();
                            removeAllComponents();
                            addComponent(hotelViewForCity);
                        });

                        Image image = new Image();
                        image.setSizeFull();
                        if (city.getImage() == null) {
                            String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
                            FileResource resource = new FileResource(new File(basepath +"/WEB-INF/images/no_image_available.jpg"));
                            image.setSource(resource);
                        } else {
                            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(city.getImage());
                            StreamResource resource = new StreamResource(() -> byteArrayInputStream, city.getName());
                            image.setSource(resource);
                            try {
                                byteArrayInputStream.close();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        }

                        TextArea descriptionArea = new TextArea();
                        descriptionArea.setWidth("100%");
                        descriptionArea.setHeight("110");
                        descriptionArea.setValue(city.getDescription());
                        descriptionArea.setReadOnly(true);
                        descriptionArea.setCaption("Опис:");

                        innerLayout.addComponents(cityNameAndAVGRatingLayout, descriptionArea, viewHotels);
                        panelLayout.addComponents(image, innerLayout);
                        panelLayout.setComponentAlignment(image, Alignment.MIDDLE_CENTER);
                        panelLayout.setExpandRatio(image, 0.2f);
                        panelLayout.setExpandRatio(innerLayout, 0.9f);
                        panel.addClickListener(clickEvent -> panelClickListener(contentLayout, outerLayout, city));
                        panel.setContent(panelLayout);
                        outerLayout.addComponent(panel);
                        contentLayout.addComponent(outerLayout);
                    });
        }
    }

    /**Function for opening {@link CityInfoView} page
     * {@param contentLayout} - stores {@param outerLayout}
     * {@param outerLayout} - stores {@link com.example.thebestrating.view.CityInfoView}
     * {@param hotel} - stores an {@link CityEntity} object, information about will be shown
     * in {@link CityInfoView}
     * */
    private void panelClickListener(VerticalLayout contentLayout, VerticalLayout outerLayout, CityEntity city) {
        if (!isOpen) {
            isOpen = true;
            cityInfoView.setCity(city);
            cityInfoView.init();
            outerLayout.addComponent(cityInfoView);
        } else {
            isOpen = false;
            outerLayout.removeComponent(cityInfoView);
            contentLayout.removeAllComponents();
            setLayoutContent(contentLayout);
        }
    }

    /**Function for changing button's sequence numbers*/
    private void refactorPageButton(Button currentButton) {
        int btnIndex = Integer.parseInt(currentButton.getCaption());
        if (currentButton == pageButton[2]) {
            int hotelsCount = cityService.getAllCities().size();
            if (hotelsCount <= btnIndex * MAX_CITIES_ON_PAGE)
                return;
            for (Button btn : pageButton) {
                int index = Integer.parseInt(btn.getCaption());
                btn.setCaption(String.valueOf(index + 1));
            }
            pageButton[1].focus();
        }
        if (currentButton == pageButton[0] && btnIndex != 1) {
            for (Button btn : pageButton) {
                int index = Integer.parseInt(btn.getCaption());
                btn.setCaption(String.valueOf(index - 1));
            }
            pageButton[1].focus();
        }
    }

    /**Function for drawing page buttons*/
    private void drawPageButton(Button[] pageButton) {
        if (cityService.getAllCities().size() <= MAX_CITIES_ON_PAGE)
            return;
        for (int i = 0; i < 3; i++) {
            if (cityService.getAllCities().size() > (Integer.parseInt(pageButton[i].getCaption())-1)
                    * MAX_CITIES_ON_PAGE) {
                pageButton[i].setVisible(true);
            }
        }
    }
}
