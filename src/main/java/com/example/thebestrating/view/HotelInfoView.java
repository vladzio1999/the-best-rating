package com.example.thebestrating.view;

import com.example.thebestrating.city.CityEntity;
import com.example.thebestrating.feedback.FeedbackEntity;
import com.example.thebestrating.feedback.FeedbackService;
import com.example.thebestrating.hotel.HotelEntity;
import com.example.thebestrating.hotel.HotelService;
import com.example.thebestrating.ui.MenuNavigation;
import com.example.thebestrating.utils.google.GoogleMapService;
import com.example.thebestrating.utils.validation.FieldValidator;
import com.example.thebestrating.utils.validation.ValidateUtils;
import com.google.maps.errors.ApiException;
import com.vaadin.annotations.Theme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.teemu.ratingstars.RatingStars;

import java.io.IOException;

/**
 * Class for adding reviews for hotels
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
@SpringComponent
@VaadinSessionScope
@Theme("valo")
public class HotelInfoView extends VerticalLayout implements View {

    /**Service to perform operations with {@link FeedbackEntity}*/
    @Autowired
    private FeedbackService feedbackService;

    /**Service to perform operations with {@link HotelEntity}*/
    @Autowired
    private HotelService hotelService;

    /**Service to perform operations with map*/
    @Autowired
    private GoogleMapService googleMapService;

    /**Variable to set {@link AddHotelView} into current layout*/
    @Autowired
    private AddHotelView addHotelView;

    /**Constant for set number of {@link FeedbackEntity} per one page*/
    private final static int MAX_FEEDBACK_COUNT = 5;

    /**Variable which store number of current {@link FeedbackEntity} page*/
    private int currentFeedbackPage = 1;

    /**Variable for store {@link HotelEntity} object, information about will be shown*/
    private HotelEntity hotel;

    /**button for changing review pages to previous*/
    private Button btnPrev;

    /**button for changing pages to the next*/
    private Button btnNext;


    /**Variable for store vaadin UI objects that are currently showing*/
    private Layout activeLayout;

    /**Button*/
    private Button activeButtonCancel;

    /**Variable for temporary storing value from @param msg */
    private String dumpMsg;

    /**Variable for temporary storing value from @param ratingStarsDraw */
    private Double dumpRate;

    /**Layout for storing set of {@link FeedbackEntity} for a given {@link HotelEntity} */
    private VerticalLayout feedbackLayout;

    /**Button for performing deletion for a given {@link HotelEntity} if there are no {@link FeedbackEntity}*/
    private Button deleteButton;

    /**Layout for storing buttons for deleting a record from database for a given {@link HotelEntity} */
    private VerticalLayout deleteLayout;

    /**Layout for storing UI components, which show information about all {@link HotelEntity} */
    private HorizontalLayout mainLayout;

    /**Button for performing edit for a given {@link HotelEntity}*/
    private Button editHotelButton;

    /**Function which call {@link HotelInfoView#loadComponents()} function*/
    protected void init() {
        loadComponents();
    }

    /**
     * Getter for hotel field
     * @return hotel entity*/
    public HotelEntity getHotel() {
        return hotel;
    }

    /**Setter for hotel field
     * @param hotel object that should be represented*/
    public void setHotel(HotelEntity hotel) {
        this.hotel = hotel;
    }

    /**Function for loading vaadin UI objects on page */
    private void loadComponents(){
        removeAllComponents();
        currentFeedbackPage = 1;

        feedbackLayout = new VerticalLayout();
        feedbackLayout.setMargin(false);
        setMargin(false);

        /**Object for storing {@param deleteButton} and  {@param confirmationLayout}*/
        deleteLayout = new VerticalLayout();
        deleteLayout.setMargin(false);
        mainLayout = new HorizontalLayout();
        mainLayout.setSizeFull();
        mainLayout.setMargin(false);

        Panel panel = new Panel();
        VerticalLayout newFeedback = new VerticalLayout();
        TextArea feedbackMsg = new TextArea();
        feedbackMsg.setCaption("Ваш коментар");
        feedbackMsg.setWidth("500px");
        ValidateUtils.addValidator(feedbackMsg, new FieldValidator(255));

        /**Button to show {@param confirmationLayout}*/
        deleteButton = new Button();
        deleteButton.setCaption("Delete");
        deleteButton.addStyleName(ValoTheme.BUTTON_DANGER);
        deleteButton.setIcon(VaadinIcons.MINUS_CIRCLE);

        /**Button to hide the {@param confirmationLayout}*/
        Button noButton = new Button("No");
        noButton.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        noButton.setWidth("100");

        /**Button for deleting {@link CityEntity} from a database*/
        Button yesButton = new Button("Yes");
        yesButton.addStyleName(ValoTheme.BUTTON_DANGER);
        yesButton.setWidth("100");

        /**Layout to store {@param yesButton}, {@param noButton} buttons*/
        CssLayout confirmationLayout = new CssLayout();
        confirmationLayout.addComponents(yesButton,noButton);
        confirmationLayout.setVisible(false);

        /**Object to store grade about {@link HotelEntity} */
        RatingStars ratingStars = new RatingStars();
        ratingStars.setAnimated(false);
        ratingStars.setMaxValue(5);
        ratingStars.setCaption("Ваша оцінка: ");

        /**Button to save {@link FeedbackEntity} about {@link HotelEntity} */
        Button saveBtn = new Button("Зберегти");
        saveBtn.setEnabled(false);

        /**Button for edit {@link HotelEntity} record in database*/
        editHotelButton = new Button("Редагувати готель");

        editHotelButton.addClickListener( e->editButtonClickFunc(hotel) );

        feedbackMsg.addValueChangeListener(valueChangeEvent -> {
            if(feedbackMsg.isEmpty() || (ratingStars.getValue() <= 0.0)) {
                saveBtn.setEnabled(false);
            } else {
                saveBtn.setEnabled(true);
            }
        });

        /**Click listener for {@param ratingStars} that save grade for {@link FeedbackEntity}
         * into a database for given  {@link HotelEntity}*/
        ratingStars.addValueChangeListener( e ->{
            if(feedbackMsg.isEmpty() || (ratingStars.getValue() <= 0.0)) {
                saveBtn.setEnabled(false);
            } else {
                saveBtn.setEnabled(true);
            }
        });

        /**Click listener for {@param saveBtn} that save {@link FeedbackEntity}
         * into a database for given  {@link HotelEntity}*/
        saveBtn.addClickListener(clickEvent -> {
            if (feedbackMsg.getComponentError() == null) {
                feedbackMsg.getComponentError();
                hotelService.addFeedback(hotel.getId(), feedbackMsg.getValue(), ratingStars.getValue());
                feedbackMsg.clear();
                feedbackMsg.setComponentError(null);
                ratingStars.setValue(null);
                currentFeedbackPage = 1;
                setHotel(hotelService.getHotel(hotel.getId()));
                feedbackLayout.removeAllComponents();
                hotel.getReversedList().stream()
                        .limit(MAX_FEEDBACK_COUNT)
                        .forEach(feedbackEntity -> feedbackLayout.addComponent(
                                drawFeedback(feedbackEntity)));
                btnPrev.setVisible(false);
                if (hotel.getFeedbackList().size() > MAX_FEEDBACK_COUNT) {
                    btnNext.setVisible(true);
                } else {
                    btnNext.setVisible(false);
                }

                deleteLayout.setVisible(false);
            }
        });

        /**Click listener to {@param deleteButton} that hide or show {@param confirmationLayout}*/
        deleteButton.addClickListener(e -> {
            confirmationLayout.setVisible(!(confirmationLayout.isVisible()));
        });

        /**Click listener to {@param yesButton} that delete {@link HotelEntity} from a database */
        yesButton.addClickListener(e -> {
            if(!(hotel.hasReviews())) {
                hotelService.deleteHotelById(hotel.getId());
                Page.getCurrent().reload();
            }
            else {
                Notification.show("You cannot delete hotel that already has some reviews!");
            }
        });

        /**Click listener to hide {@param confirmationLayout} */
        noButton.addClickListener(e -> {
            confirmationLayout.setVisible(false);
        });

        /**Click listener for {@param editHotelButton} that call {@link HotelInfoView#editButtonClickFunc} function */
        editHotelButton.addClickListener( e->editButtonClickFunc(hotel) );

        newFeedback.addComponents(editHotelButton, feedbackMsg, ratingStars, saveBtn);
        newFeedback.addLayoutClickListener(contextClickEvent -> {
            if(activeLayout!=null) {
                activeButtonCancel.click();
            }

        });
        panel.setWidth("550px");
        panel.setContent(newFeedback);

        feedbackLayout.setDefaultComponentAlignment(Alignment.TOP_CENTER);
        hotel.getReversedList().stream()
                .limit(MAX_FEEDBACK_COUNT)
                .forEach(feedbackEntity -> feedbackLayout.addComponent(
                        drawFeedback(feedbackEntity)));


        VerticalLayout components = new VerticalLayout();
        components.setMargin(false);
        components.setDefaultComponentAlignment(Alignment.TOP_CENTER);
        components.addComponents(feedbackLayout, createNavigation(feedbackLayout));

        if(hotel.hasReviews()) deleteLayout.setVisible(false);

        deleteLayout.addComponents(deleteButton, confirmationLayout);
        deleteLayout.setComponentAlignment(deleteButton, Alignment.MIDDLE_RIGHT);
        deleteLayout.setComponentAlignment(confirmationLayout, Alignment.MIDDLE_RIGHT);

        VerticalLayout mapAndFeedback = new VerticalLayout();
        mapAndFeedback.setMargin(false);
        mapAndFeedback.addComponent(panel);

        GoogleMap googleMap = googleMapService.googleMapInit();

        googleMap.setHeight("300px");
        googleMap.setWidth("550px");
        GoogleMapMarker mapMarker = new GoogleMapMarker();
        mapMarker.setPosition(hotel.getLatLon());
        googleMap.setCenter(mapMarker.getPosition());
        googleMap.addMarker(mapMarker);
        setDefaultComponentAlignment(Alignment.TOP_CENTER);

        mapAndFeedback.addComponent(googleMap);

        mainLayout.addComponents(components, mapAndFeedback, deleteLayout);
        mainLayout.setComponentAlignment(deleteLayout, Alignment.TOP_RIGHT);
        mainLayout.setSizeFull();

        addComponents(mainLayout);
    }

    /**
     * Function for drawing reviews on page
     * @param feedback - review that will be drawn
     * @return field with feedback
     */
    private Layout drawFeedback(FeedbackEntity feedback) {
        AbsoluteLayout layout = new AbsoluteLayout();
        RatingStars ratingStarsDraw = new RatingStars();
        ratingStarsDraw.setValue(feedback.getGrade());
        ratingStarsDraw.setReadOnly(true);
        ratingStarsDraw.setAnimated(false);
        ratingStarsDraw.setMaxValue(5);

        layout.addLayoutClickListener( layoutClickEvent -> {
            if(layout != activeLayout && activeLayout!=null) {
                activeLayout = null;
                activeButtonCancel.click();
            }
        });
        layout.setWidth("578px");
        layout.setHeight("216px");

        TextArea msg = new TextArea("Unknown user");
        msg.setWidth("500px");
        msg.setValue(feedback.getMessage());
        msg.setReadOnly(true);


        ValidateUtils.addValidator(msg, new FieldValidator(255));
        Button btnSave = new Button("Зберегти");

        Button btnCancel = new Button("Скасувати");
        btnCancel.addClickListener(clickEvent -> {
            btnSave.setVisible(false);
            btnCancel.setVisible(false);
            msg.setValue(dumpMsg);
            msg.setReadOnly(true);
            ratingStarsDraw.setValue(dumpRate);
            ratingStarsDraw.setReadOnly(true);
        });

        btnSave.addClickListener(clickEvent -> {
            if (msg.getComponentError() ==null) {
                activeLayout = null;
                dumpMsg = msg.getValue();
                dumpRate = ratingStarsDraw.getValue();
                feedbackService.update(feedback.getId(), dumpMsg, dumpRate);
                btnCancel.click();
            } else {
                Notification notification = new Notification("Заповніть поле правильно!");
                notification.show(Page.getCurrent());
            }
        });
        btnSave.setVisible(false);
        btnCancel.setVisible(false);

        MenuBar moreMenu = new MenuBar();
        moreMenu.setHeight(40, Unit.PIXELS);
        MenuBar.MenuItem more = moreMenu.addItem("...", null);
        more.addItem("Редагувати", null, menuItem -> {
            activeLayout = layout;
            activeButtonCancel = btnCancel;
            dumpMsg = msg.getValue();
            dumpRate = ratingStarsDraw.getValue();
            msg.setReadOnly(false);
            ratingStarsDraw.setReadOnly(false);
            msg.focus();
            btnSave.setVisible(true);
            btnCancel.setVisible(true);
        });
        more.addItem("Видалити", null, menuItem -> ConfirmDialog.show(getUI(),
                "Будь ласка, підтверідіть:", "Ви дійсно хочете видалити цей коментар?"
                , "Так", "Ні", (ConfirmDialog.Listener) confirmDialog -> {
                    if(confirmDialog.isConfirmed()) {
                        feedbackService.deleteById(feedback.getId());
                        setHotel(hotelService.getHotel(hotel.getId()));
                        feedbackLayout.removeAllComponents();
                        hotel.getReversedList().stream()
                                .skip((currentFeedbackPage-1)*MAX_FEEDBACK_COUNT)
                                .limit(MAX_FEEDBACK_COUNT)
                                .forEach(feedbackEntity -> feedbackLayout.addComponent(drawFeedback(feedbackEntity)));
                        if(feedbackLayout.getComponentCount() ==0 && currentFeedbackPage!=1)
                            btnPrev.click();
                        if (currentFeedbackPage * MAX_FEEDBACK_COUNT >= hotel.getFeedbackList().size())
                            btnNext.setVisible(false);
                        if (!hotel.hasReviews()) {
                            deleteLayout.setVisible(true);
                        }
                    }
                }));

        layout.addComponent(moreMenu, "right: 0px; top: 55px;");
        setMargin(false);
        layout.addComponent(msg, "top: 55px; left: 20px;");
        layout.addComponent(ratingStarsDraw, "top: 20px; left: 415px;");
        layout.addComponent(btnSave, "top: 178px; left: 20px;");
        layout.addComponent(btnCancel, "top: 178px; left: 120px;");
        return layout;
    }

    /**
     * Function for changing pages
     * @param feedbackLayout - will store set of reviews
     * @return field with navigation for feedbacks
     */
    public HorizontalLayout createNavigation(VerticalLayout feedbackLayout) {
        HorizontalLayout navigation = new HorizontalLayout();
        navigation.setDefaultComponentAlignment(Alignment.TOP_CENTER);

        btnNext = new Button();
        btnPrev = new Button();

        btnNext.addStyleNames(ValoTheme.BUTTON_LINK);
        btnPrev.addStyleNames(ValoTheme.BUTTON_LINK);

        btnNext.setIcon(VaadinIcons.ANGLE_RIGHT);
        btnPrev.setIcon(VaadinIcons.ANGLE_LEFT);

        btnNext.setVisible(false);

        if (currentFeedbackPage * MAX_FEEDBACK_COUNT < hotel.getFeedbackList().size()) {
            btnNext.setVisible(true);
        }

        if (currentFeedbackPage ==1) {
            btnPrev.setVisible(false);
        }

        btnNext.addClickListener(clickEvent -> {
            feedbackLayout.removeAllComponents();
            hotel.getReversedList().stream()
                    .skip(currentFeedbackPage * MAX_FEEDBACK_COUNT)
                    .limit(MAX_FEEDBACK_COUNT)
                    .forEach(feedbackEntity ->
                            feedbackLayout.addComponent(drawFeedback(feedbackEntity)));
            currentFeedbackPage++;

            if (currentFeedbackPage * MAX_FEEDBACK_COUNT >= hotel.getFeedbackList().size()) {
                btnNext.setVisible(false);
            }
            btnPrev.setVisible(true);
        });


        btnPrev.addClickListener(clickEvent -> {
            currentFeedbackPage--;
            feedbackLayout.removeAllComponents();

            if (currentFeedbackPage == 1) {
                btnPrev.setVisible(false);
            }

            hotel.getReversedList().stream()
                    .skip((currentFeedbackPage-1) * MAX_FEEDBACK_COUNT)
                    .limit(MAX_FEEDBACK_COUNT)
                    .forEach(feedbackEntity ->
                            feedbackLayout.addComponent(drawFeedback(feedbackEntity)));
            btnNext.setVisible(true);
        });
        navigation.setMargin(false);
        navigation.addComponents(btnPrev, btnNext);
        return navigation;
    }

    /**
     * Function to change current page to a {@link AddHotelView} page, which also allow edit {@link HotelEntity}
     * @param hotel - {@link HotelEntity} class object
     */
    private void editButtonClickFunc(HotelEntity hotel){

        addHotelView.setHotelEntity(hotel);
        removeAllComponents();
        try {
            ((MenuNavigation)UI.getCurrent()).goToAddHotel();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
