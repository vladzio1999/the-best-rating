package com.example.thebestrating.city;

import com.example.thebestrating.feedback.FeedbackEntity;
import com.example.thebestrating.hotel.HotelEntity;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Entity class for City
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
@Entity
public class CityEntity {

    /** hotel's id*/
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** Variable that store the name of the city*/
    private String name;

    /** Variable that store the description of the city*/
    private String description;

    /** Array that store the image of the city*/
    @Type(type = "org.hibernate.type.ImageType")
    private byte[] image;

    /** List that store reviews of the city*/
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="city_id")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<FeedbackEntity> feedbackList = new ArrayList<>();

    /** List that store hotels of the city*/
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "city")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<HotelEntity> hotelsList = new ArrayList<>();

    /** Default constructor*/
    public CityEntity() {
    }

    /**constructor that initialize {@link CityEntity} object
     * @param id - identification number
     * @param name - name of the City
     * @param description - description of the City*/
    public CityEntity(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    /**Getter for id field
     * @return identification number */
    public Long getId() {
        return id;
    }

    /**Setter for id field
     * @param id - identification number */
    public void setId(Long id) {
        this.id = id;
    }

    /**Getter for name field
     * @return city name */
    public String getName() {
        return name;
    }

    /**Setter for name field
     * @param name - city name */
    public void setName(String name) {
        this.name = name;
    }

    /**Getter for description field
     * @return  city description */
    public String getDescription() {
        return description;
    }

    /**Setter for description field
     * @param description - description about city */
    public void setDescription(String description) {
        this.description = description;
    }

    /**Getter for feedbackList field
     * @return list of feedbacks */
    public List<FeedbackEntity> getFeedbackList() {
        return feedbackList;
    }

    /**Get reversed description list
     * @return  reversed description list */
    public List<FeedbackEntity> getReversedList() {
        List temp = new ArrayList(getFeedbackList());
        Collections.reverse(temp);
        return  temp;
    }

    /**Get average grade
     * @return  average grade */
    public Double getAvgRating(){
        Double sum = 0.0;
        for(FeedbackEntity feedback : feedbackList){
            sum += feedback.getGrade();
        }
        return sum/feedbackList.size();
    }

    /**Check for reviews
     * @return  true if there are review, false otherwise*/
    public boolean hasReviews(){
        return !(feedbackList.isEmpty());
    }

    /**Check for hotels
     * @return  true if there are hotels, false otherwise*/
    public boolean hasHotels() {
        return !(hotelsList.isEmpty());
    }

    /**Getter for image field
     * @return array of bytes */
    public byte[] getImage() {
        return image;
    }

    /**Setter for image field
     * @param image - array of bytes */
    public void setImage(byte[] image) {
        this.image = image;
    }

    /**Comparison of objects of this class
     * @param o - object to be compared
     * @return true if objects are similar*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CityEntity)) return false;
        CityEntity that = (CityEntity) o;
        return id.equals(that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description);
    }

    /**Getting hashcode of object
     * @return hash code of hotel*/
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**Converting object of class into string
     * @return object converted to string*/
    @Override
    public String toString() {
        return "CityEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '}';
    }
}
