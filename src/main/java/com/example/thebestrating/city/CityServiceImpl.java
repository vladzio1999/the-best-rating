package com.example.thebestrating.city;

import com.example.thebestrating.feedback.FeedbackEntity;
import com.example.thebestrating.feedback.FeedbackService;
import com.example.thebestrating.feedback.FeedbackServiceImpl;
import com.example.thebestrating.hotel.HotelEntity;
import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Implementation class for {@link CityService} interface
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
@Service
@VaadinSessionScope
public class CityServiceImpl implements CityService {

    /**Repository for cities*/
    @Autowired
    CityRepository cityRepository;

    /**Service for cities*/
    @Autowired
    FeedbackService feedbackService;

    /**Obtaining city
     * @param id - identification number
     * @return city by given ID*/
    @Override
    public CityEntity getCity(Long id) {
        if(cityRepository.findById(id).isPresent()){
            return cityRepository.findById(id).get();
        }
        return null;
    }

    /**Obtaining all cities
     * @return all cities from database*/
    @Override
    public List<CityEntity> getAllCities() {
        return new ArrayList<>(cityRepository.findAll());
    }

    /**Creating city
     * @param name - name of the city
     * @param description - description of the city
     * @param image - byte array that stores image*/
    @Override
    public void createCity(String name, String description, byte[] image) {
        CityEntity cityEntity = new CityEntity();
        cityEntity.setName(name);
        cityEntity.setDescription(description);
        cityEntity.setImage(image);
        cityRepository.save(cityEntity);
    }

    /**Deleting city by given id
     * @param id - identification number*/
    @Override
    public void deleteCityById(Long id) {
        cityRepository.deleteById(id);
    }

    /**get city by it's name
     * @param name - name of the city
     * @return city*/
    @Override
    public CityEntity getCityByName(String name){
        return cityRepository.findCityByName(name);
    }

    /**adding feedback to the city
     * @param cityId - identification number
     * @param message - feedback text
     * @param grade - grade
     * @return feedback that has been added to the city*/
    @Override
    public FeedbackEntity addFeedback(Long cityId, String message, Double grade) {
        CityEntity city = getCity(cityId);
        city.getFeedbackList().add(feedbackService.createFeedback(message,grade));
        city = cityRepository.save(city);
        return city.getFeedbackList().get(city.getFeedbackList().size()-1);
    }

    /**save new city to the database
     * @param cityEntity - city that will be saved
     * @return saved city*/
    @Override
    public CityEntity save(CityEntity cityEntity) {
        return cityRepository.save(cityEntity);
    }
}
