package com.example.thebestrating.city;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface to save objects of {@link com.example.thebestrating.city.CityEntity}
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 */
@Repository
public interface CityRepository extends JpaRepository<CityEntity, Long> {

    /**Function for finding City by given name
     * @param name - city nanem
     * @return city*/
    CityEntity findCityByName(String name);
}
