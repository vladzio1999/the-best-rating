package com.example.thebestrating.city;

import com.example.thebestrating.feedback.FeedbackEntity;

import java.util.List;


/**
 * Interface describing the functionality of the service
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
public interface CityService {

    /**Obtaining city
     * @param id - identification number
     * @return city by given ID*/
    CityEntity getCity(Long id);

    /**Obtaining all cities
     * @return all cities from database*/
    List<CityEntity> getAllCities();

    /**Creating city
     * @param name - name of the city
     * @param description - description of the city
     * @param image - byte array that stores image*/
    void createCity(String name, String description, byte[] image);

    /**Deleting city
     * @param id - identification number*/
    void deleteCityById(Long id);

    /**get city by it's name
     * @param name - name of the city
     * @return city*/
    CityEntity getCityByName(String name);

    /**adding feedback to the city
     * @param cityId - identification number
     * @param message - feedback text
     * @param grade - grade
     * @return feedback that has been added to the city*/
    FeedbackEntity addFeedback(Long cityId, String message, Double grade);

    /**save new city to the database
     * @param cityEntity - city that will be saved
     * @return saved city*/
    CityEntity save(CityEntity cityEntity);
}
