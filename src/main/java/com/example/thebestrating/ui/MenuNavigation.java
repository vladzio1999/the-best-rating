package com.example.thebestrating.ui;

import com.google.maps.errors.ApiException;

import java.io.IOException;

/**
 * Interface for navigating on site
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
public interface MenuNavigation {

    /** method to set CityView inside layout*/
    void goToAllCities();

    /** method to set AddCityView inside layout*/
    void goToAddCity();

    /**
     * method to set AddHotelView inside layout
     * @throws InterruptedException when a thread is waiting
     * @throws ApiException when error occurred while consuming a provider API
     * @throws IOException signals that an I/O exception of some sort has occurred
     */
    void goToAddHotel() throws InterruptedException, ApiException, IOException;

    /** method to set HotelView inside layout*/
    void goToAllHotels();
}
