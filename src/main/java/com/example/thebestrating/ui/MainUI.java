package com.example.thebestrating.ui;

import com.example.thebestrating.view.*;
import com.google.maps.errors.ApiException;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.hibernate.mapping.Constraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.io.File;
import java.io.IOException;


/**
 * Class for navigating on site
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
@SpringUI(path = "")
@Theme("valo")
@VaadinSessionScope
@SpringComponent
@Widgetset("wsb8a159e28a39dc93411bcb5277d4616e")
public class MainUI extends UI implements MenuNavigation{

    /**Field containing all the content*/
    private VerticalLayout mainLayout;
    /**Field containing navigation menu bar*/
    private HorizontalLayout titleLayout;
    /**Field containing information about hotels or cities*/
    private VerticalLayout contentLayout;

    /**Page for watching hotel list*/
    @Autowired
    private HotelView hotelView;
    /**Page for adding hotel*/
    @Autowired
    private AddHotelView addHotelView;
    /**Page for watching city list*/
    @Autowired
    private CityView cityView;
    /**Page for adding city*/
    @Autowired
    private AddCityView addCityView;

    /**
     * Method to initialize page
     * @param vaadinRequest - vaadin request
     */
    @Override
    public void init(VaadinRequest vaadinRequest) {
        getPage().setTitle("The Best Rating");
        mainLayout = new VerticalLayout();
        mainLayout.setMargin(false);
        titleLayout = new HorizontalLayout();
        contentLayout = new VerticalLayout();
        contentLayout.setMargin(false);

        String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
        FileResource resource = new FileResource(new File(basepath +"/WEB-INF/images/hotel-icon.jpg"));
        Image image = new Image(null, resource);
        image.setHeight("60");
        image.setWidth("90");

        Label titleLabel = new Label("The Best Rating");
        titleLabel.setStyleName(ValoTheme.LABEL_H1);

        MenuBar mainBar = new MenuBar();
        mainBar.setAutoOpen(true);
        setMenuBar(mainBar);

        titleLayout.addComponents(image, titleLabel, mainBar);
        titleLayout.setSizeFull();
        titleLayout.setHeight("90");
        titleLayout.setStyleName(ValoTheme.MENU_TITLE);
        titleLayout.setExpandRatio(image, 0.1f);
        titleLayout.setExpandRatio(titleLabel, 0.1f);
        titleLayout.setExpandRatio(mainBar, 0.8f);
        titleLayout.setComponentAlignment(image, Alignment.BOTTOM_LEFT);
        titleLayout.setComponentAlignment(titleLabel, Alignment.BOTTOM_LEFT);
        titleLayout.setComponentAlignment(mainBar, Alignment.MIDDLE_RIGHT);
        hotelView.init();
        contentLayout.addComponent(hotelView);
        contentLayout.setSizeFull();

        mainLayout.addComponents(titleLayout, contentLayout);
        setContent(mainLayout);
    }

    /**Method to set navigation menu bar
     * @param menuBar - object of menu bar
     */
    private void setMenuBar(MenuBar menuBar) {
        MenuBar.MenuItem mainPage = menuBar.addItem("Головна сторінка", null, menuItem -> goToAllHotels() );
        MenuBar.MenuItem hotels = menuBar.addItem("Готелі");
        hotels.addItem("Додати готель", null, menuItem -> {
            try {
                goToAddHotel();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ApiException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        MenuBar.MenuItem cities = menuBar.addItem("Міста");
        cities.addItem("Переглянути міста", null, menuItem -> goToAllCities() );
        cities.addItem("Додати місто", null, menuItem -> goToAddCity() );
    }

    /**Method to switch to page view hotels*/
    @Override
    public void goToAllHotels(){
        contentLayout.removeAllComponents();
        hotelView.init();
        contentLayout.addComponent(hotelView);
    }

    /**Method to switch to page for adding hotels*/
    @Override
    public void goToAddHotel() throws InterruptedException, ApiException, IOException {
        contentLayout.removeAllComponents();
        addHotelView.init();
        contentLayout.addComponent(addHotelView);
    }

    /**Method to switch to page view cities*/
    @Override
    public void goToAllCities() {
        contentLayout.removeAllComponents();
        cityView.init();
        contentLayout.addComponent(cityView);
    }

    /**Method to switch to page for adding cities*/
    @Override
    public void goToAddCity() {
        contentLayout.removeAllComponents();
        addCityView.init();
        contentLayout.addComponent(addCityView);
    }
}
