package com.example.thebestrating.hotel;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Class to save objects of {@link com.example.thebestrating.hotel.HotelEntity}
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 */
@Repository
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface HotelRepository extends JpaRepository<HotelEntity, Long> {
    List<HotelEntity> findHotelsByCityName(String name);
}
