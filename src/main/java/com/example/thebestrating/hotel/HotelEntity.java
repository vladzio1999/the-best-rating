package com.example.thebestrating.hotel;

import com.example.thebestrating.city.CityEntity;
import com.example.thebestrating.feedback.FeedbackEntity;
import com.vaadin.spring.annotation.VaadinSessionScope;
import org.hibernate.annotations.Type;
import com.vaadin.tapio.googlemaps.client.LatLon;
import org.hibernate.mapping.Collection;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Class is entity of hotel
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
@Entity
@VaadinSessionScope
@Table(name = "hotel")
public class HotelEntity {

    /**Identification number of hotel*/
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**Name of hotel*/
    private String name;
    /**Description of hotel*/
    private String description;
    /**Image of hotel*/
    @Type(type = "org.hibernate.type.ImageType")
    private byte[] image;

    /**City of hotel*/
    @ManyToOne
    private CityEntity city;

    /**Feedbacks of hotel*/
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="hotel_id")
    private List<FeedbackEntity> feedbackList = new ArrayList<>();

    /**Coordinates of hotel*/
    private LatLon latLon;
    /**Address of hotel*/
    private String address;

    /**Constructor creating new object
     * @see HotelEntity#HotelEntity(Long, String, String, CityEntity)
     */
    public HotelEntity() {}

    /**Constructor creating new object with specific values
     * @see HotelEntity#HotelEntity()
     * @param id - identification number
     * @param name - name of hotel
     * @param description - description of hotel
     * @param city - city of hotel
     */
    public HotelEntity(Long id, String name, String description , CityEntity city) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.city = city;
    }

    /**
     * Obtaining a field value id that can be specified by a method {@link HotelEntity#setId(Long)}
     * @return value of field id
     */
    public Long getId() {
        return id;
    }

    /**
     * Setting the value of the field id that can be obtained by a method
     * {@link HotelEntity#getId()}
     * @param id - hotel id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Obtaining a field value name that can be specified by a method {@link HotelEntity#setName(String)}
     * @return value of field name
     */
    public String getName() {
        return name;
    }

    /**
     * Setting the value of the field name that can be obtained by a method
     * {@link HotelEntity#getName()}
     * @param name - hotel name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Obtaining a field value description that can be specified by a method
     * {@link HotelEntity#setDescription(String)}
     * @return value of field description
     */
    public String getDescription() {
        return description;
    }

    /**Setting the value of the field description that can be obtained by a method
     * {@link HotelEntity#getDescription()}
     * @param description - hotel description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Obtaining a field value image that can be specified by a method {@link HotelEntity#setImage(byte[])}
     * @return value of field image
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Setting the value of the field image that can be obtained by a method
     * {@link HotelEntity#getImage()} )}
     * @param image - image of hotel
     */
    public void setImage(byte[] image) {
        this.image = image;
    }

    /**
     * Obtaining a field value feedBackList that can be specified by a method
     * {@link HotelEntity#setFeedbackList(List)}
     * @return value of field feedBackList
     */
    public List<FeedbackEntity> getFeedbackList() {
        return this.feedbackList;
    }

    /**
     * Obtaining a reversed feedBackList
     * @return temporary list of feedback
     */
    public List<FeedbackEntity> getReversedList() {
        List temp = new ArrayList(getFeedbackList());
        Collections.reverse(temp);
        return  temp;
    }

    /**
     * Obtaining an average rating of hotel
     * @return value of average rating
     */
    public Double getAvgRating(){
        Double sum = 0.0;
        for(FeedbackEntity feedback : feedbackList){
            sum +=feedback.getGrade();
        }
        return sum/feedbackList.size();
    }

    /**
     * Obtaining a field value city that can be specified by a method {@link HotelEntity#setCity(CityEntity)}
     * @return value of field city
     */
    public CityEntity getCity() {
        return city;
    }

    /**
     * Setting the value of the field city that can be obtained by a method
     * {@link HotelEntity#getCity()}
     * @param city - city
     */
    public void setCity(CityEntity city) {
        this.city = city;
    }

    /**Checking if hotel has feedbacks
     * @return result of the check
     */
    public boolean hasReviews(){
        return !(feedbackList.isEmpty());
    }

    /**
     * Setting the value of the field feedBackList that can be obtained by a method
     * {@link HotelEntity#getFeedbackList()}
     * @param feedbackList - hotel feedbacks
     */
    public void setFeedbackList(List<FeedbackEntity> feedbackList) {
        this.feedbackList = feedbackList;
    }


    /**
     * Obtaining a field value latTon that can be specified by a method {@link HotelEntity#setLatLon(LatLon)}
     * @return value of field latTon
     */
    public LatLon getLatLon() {
        return latLon;
    }

    /**
     * Setting the value of the field latTon that can be obtained by a method
     * {@link HotelEntity#getLatLon()}
     * @param latLon - hotel coordinates
     */
    public void setLatLon(LatLon latLon) {
        this.latLon = latLon;
    }

    /**
     * Obtaining a field value address that can be specified by a method {@link HotelEntity#setAddress(String)}
     * @return value of field address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setting the value of the field address that can be obtained by a method
     * {@link HotelEntity#getAddress()}
     * @param address - hotel address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Comparison of objects of this class
     * @param o - object
     * @return result of comparison
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HotelEntity that = (HotelEntity) o;
        return id.equals(that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(city, that.city) &&
                Objects.equals(feedbackList, that.feedbackList);
    }

    /**
     * Getting hashcode of object
     * @return hash code of hotel
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**
     * Converting object of class into string
     * @return string representation of the hotel
     */
    @Override
    public String toString() {
        return "HotelEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", city=" + city +
                ", feedbackList=" + feedbackList +
                '}';
    }
}
