package com.example.thebestrating.hotel;

import com.example.thebestrating.city.CityService;
import com.example.thebestrating.feedback.FeedbackEntity;
import com.example.thebestrating.feedback.FeedbackService;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.tapio.googlemaps.client.LatLon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Class implemented interface {@link com.example.thebestrating.hotel.HotelService}
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 * */
@Service
@VaadinSessionScope
public class HotelServiceImpl implements HotelService {

    /**Repository for hotels*/
    @Autowired
    private HotelRepository hotelRepository;

    /**Service to work with cities*/
    @Autowired
    private CityService cityService;

    /**Service to work with feedbacks*/
    @Autowired
    private FeedbackService feedbackService;

    /**Obtaining hotel from repository*/
    @Override
    public HotelEntity getHotel(Long id) {
        if(hotelRepository.findById(id).isPresent()){
            return hotelRepository.findById(id).get();
        }
        return null;
    }

    /**
     * Obtaining all hotels from repository
     * @return list of hotels
     */
    @Override
    public List<HotelEntity> getAllHotels() {
        return new ArrayList<>(hotelRepository.findAll());
    }

    /**Creating hotel*/
    @Override
    public void createHotel(String name, String description, String city, byte[] image,
                            LatLon latLon, String address ) {
        HotelEntity hotelEntity = new HotelEntity();
        hotelEntity.setName(name);
        hotelEntity.setDescription(description);
        hotelEntity.setCity(cityService.getCityByName(city));
        hotelEntity.setImage(image);
        hotelEntity.setLatLon(latLon);
        hotelEntity.setAddress(address);
        hotelRepository.save(hotelEntity);
    }

    /**
     * Saving hotel to repository
     * @return hotel entity
     */
    @Override
    public HotelEntity save(HotelEntity hotel) {
        return hotelRepository.save(hotel);
    }

    /**Deleting hotel from repository*/
    @Override
    public void deleteHotelById(Long id){
        hotelRepository.deleteById(id);
    }

    /**
     * Obtaining hotel by city
     * @return list of hotels
     */
    @Override
    public List<HotelEntity> getHotelsByCity(String name){
        return hotelRepository.findHotelsByCityName(name);
    }

    /**
     * Adding feedback
     * @return feedback entity
     */
    @Override
    public FeedbackEntity addFeedback(Long hotelId, String message, Double grade) {
        HotelEntity hotel = getHotel(hotelId);
        hotel.getFeedbackList().add(feedbackService.createFeedback(message,grade));
        hotel = hotelRepository.save(hotel);
        return hotel.getFeedbackList().get(hotel.getFeedbackList().size() - 1);
    }
}
