package com.example.thebestrating.hotel;

import com.example.thebestrating.feedback.FeedbackEntity;
import com.vaadin.tapio.googlemaps.client.LatLon;

import java.util.List;

/**
 * Interface describing the functionality of the service
 * @author Vlad Tereschuk
 * @author Nazar Dochylo
 * @author Andriy Drobot
 */
public interface HotelService {
    /**
     * Obtaining hotel
     * @param id - hotel id
     * @return hotel entity
     */
    HotelEntity getHotel(Long id);
    /**
     * Obtaining all hotels
     * @return list of hotels
     */
    List<HotelEntity> getAllHotels();
    /**
     * Creating hotel
     * @param name - hotel name
     * @param description - hotel description
     * @param city -  city
     * @param image - image of hotel
     * @param latLon - hotel coordinates
     * @param address - hotel address
     */
    void createHotel(String name, String description, String city, byte[] image, LatLon latLon, String address);
    /**
     * Saving hotel
     * @param hotel - entity of hotel
     * @return hotel entity
     */
    HotelEntity save(HotelEntity hotel);
    /**
     * Deleting hotel
     * @param id - hotel id
     */
    void deleteHotelById(Long id);
    /**
     * Obtaining hotel by city
     * @param name - city name
     * @return list of hotel
     */
    List<HotelEntity> getHotelsByCity(String name);
    /**
     * Adding feedback
     * @param hotelId - hotel id
     * @param message - text of feedback
     * @param rate - hotel rating
     * @return feedback entity
     */
    FeedbackEntity addFeedback(Long hotelId, String message, Double rate);
}
